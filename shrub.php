<?php
/**
 * Shrub PHP Framework
 * Inspired by Sinatra
 * DSL-Hearted
 *
 * @author Shedrach Okonofua <shedrachokonofua@gmail.com>
 * @category Core
 * @copyright 2014, Shedrach Okonofua
 * @package Shrub
 * @version Release: Alpha
 *
 */


namespace Shrub
{
    function getroot()
    {
        return __DIR__;
    }
    function template()
    {
        return new Template();
    }
    function model($name)
    {
        include_once getroot().'/lib/'.$name.'.php';
    }
    function get($uri, $requested)
    {
        $url = str_replace($requested, '', $uri);
        $get = array();
        if(stristr($url, '?')) {
            $broken1 = explode('?', $url);
            if(stristr($broken1[1], '&')) {
                $keyandvals = explode('&', $broken1[1]);
                foreach($keyandvals as $keyandval) {
                    $keyandvals1[] = explode('=', $keyandval);
                }
                foreach($keyandvals1 as $keyandval) {
                    $get[$keyandval[0]] = $keyandval[1];
                }
            }
            else {
                $keyandvals1 = explode('=', $broken1[1]);
                for($i=0;$i<count($keyandvals1);$i++){
                    if($i != count($keyandvals1)-1) {
                        $get[$keyandvals1[$i]] = $keyandvals1[$i+1];
                    }
                }
            }
        }
        else {
            $get = array();
        }
        return $get;


    }
    class Template {
        /**
         * @var array Holder For Template.
         */
        var $temp;
        var $vars;

        function __construct() {
            $this->vars = array('Shrub' => '1.0.0');
        }
        function load($name)
        {
            $this->temp = file_get_contents(getroot().'/template/'.$name.'.html');
            return $this;
        }
        function set($vars) {
            $this->vars = $vars;
            return $this;
        }
        function parse() {
            $temp = '';
            foreach($this->vars as $key => $val) {
                $temp = str_replace('{{'.$key.'}}', $val, $this->temp);
            }
            print($temp);
        }
    }

    class App
    {
        /**
         * @var array Holder For Routes.
         */
        private $routes = array();

        /**
         * @var array Holder For Route Callbacks.
         */
        private $callback = array();

        /**
         * @var string Requested Route.
         */
        private $request = '';

        /**
         * @var string Root Directory of Shrub.
         */
        private $root = '';

        /**
         * @var array Keys in Routing
         */
        private $keys = array();

        /**
         * @var array Positions in Routing
         */
        private $positions = array();
        /**
         * Directory Where Static File Will be Served From.
         */
        private $staticDir = "";

        public function __construct() {
            $this->staticDir = $this->root.'/public';
            $this->request = isset($_GET['request']) ? $_GET['request'] : '/';
            $this->root = getroot();

            $this->keys['get'] = array();
            $this->positions['get'] = array();
            $this->routes['get'] = array();
            $this->callback['get'] = array();

            $this->keys['post'] = array();
            $this->positions['post'] = array();
            $this->routes['post'] = array();
            $this->callback['post'] = array();
        }
        /**
         * Assign route for GET Method.
         * @param String $url Route to be matched.
         * @param Method $callback The Method to be called when route is matched.
         */
        public function get($url, $callback)
        {
            $oUrl = $url;
            $url = str_replace("/", "\/", $url);
            $url = "/^".$url."$/";
            $tUrl = $url;
            $url = preg_replace("{:[A-Za-z0-9]+}", "[A-Za-z0-9]+", $url);
            $temp = array();
            preg_match_all("{:[A-Za-z0-9]+}", $tUrl, $temp);
            $this->keys['get'][$url] = $temp[0];
            $this->routes['get'][] = $url;
            $this->callback['get'][$url] = $callback;

            for($i = 0; $i <= count($this->keys['get'][$url])-1; $i++) {
                $this->keys['get'][$url][$i] = str_replace(":", "", $this->keys['get'][$url][$i]);
            }

            $broken = explode("/", $oUrl);
            array_shift($broken);
            $this->positions['get'][$url] = array();
            for($i=0; $i < count($broken); $i++) {
                if(preg_match("/^:[A-Za-z0-9]+$/", $broken[$i])) {
                    $this->positions['get'][$url][] = $i;
                }
            }
        }
        /**
         * Assign route for POST Method.
         * @param String $url Route to be matched.
         * @param Method $callback The Method to be called when route is matched.
         */
        public function post($url, $callback)
        {
            $oUrl = $url;
            $url = str_replace("/", "\/", $url);
            $url = "/^".$url."$/";
            $tUrl = $url;
            $url = preg_replace("{:[A-Za-z0-9]+}", "[A-Za-z0-9]+", $url);
            $temp = array();
            preg_match_all("{:[A-Za-z0-9]+}", $tUrl, $temp);
            $this->keys['post'][$url] = $temp[0];
            $this->routes['post'][] = $url;
            $this->callback['post'][$url] = $callback;

            for($i = 0; $i <= count($this->keys['post'][$url])-1; $i++) {
                $this->keys['post'][$url][$i] = str_replace(":", "", $this->keys['post'][$url][$i]);
            }

            $broken = explode("/", $oUrl);
            array_shift($broken);
            $this->positions['post'][$url] = array();
            for($i=0; $i < count($broken); $i++) {
                if(preg_match("/^:[A-Za-z0-9]+$/", $broken[$i])) {
                    $this->positions['post'][$url][] = $i;
                }
            }
        }
        /**
         * Run Application
         */
        public function run()
        {

            $url = $this->request;
            $requested = false;
            $method = strtolower($_SERVER['REQUEST_METHOD']);
            $routes = $this->routes[$method];
            foreach($routes as $route) {
                if(preg_match($route, $url)) {
                    $requested = $route;
                    break;
                }
            }
            if(!$requested) {
                if(file_exists($this->root.'/public'.$url) && $url != '/') {
                    $filename = $this->root.'/public'.$url;
                    $extension = explode(".", $filename);
                    $extension = $extension[count($extension)-1];
                    $extensions = array(
                        "js" => "text/javascript",
                        "css" => "text/css",
                        "mp3" => "audio/mp3",
                        "html" => "text/html",
                        "htm" => "text/html",
                        "png" => "image/png",
                        "jpg" => "image/jpeg",
                        "gif" => "image/gif",
                        "jpeg" => "image/jpeg",
                    );
                    print(file_get_contents($filename));

                    header("Content-type: ".$extensions[$extension]);
                }
                else {
                    print("404");
                }
            }
            else {
                $broken = explode("/", $url);
                array_shift($broken);
                $keyCount = count($this->keys[$method][$requested]);
                $brokenCount = count($broken);
                $callback = $this->callback[$method][$requested];
                $keys = $this->keys[$method][$requested];
                $params = array();
                $positions = $this->positions[$method][$requested];

                for($i = 0; $i<count($positions); $i++) {
                    $params[] = $broken[$positions[$i]];
                }
                $keys[] = "Timestamp";
                $params[] = time();
                $data = array_combine($keys, $params);
                $data['get'] = get($_SERVER['REQUEST_URI'], $url);
                $callback($data);
            }
        }
    }
}
